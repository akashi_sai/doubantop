# 引入命令行执行函数模块
from scrapy.cmdline import execute
import sys
import os

# 调试测试

# 获取项目文件位置
# 思路：获取当前位置文件的父文件目录
path = os.path.dirname(os.path.abspath(__file__))
# 设置项目的运行起始位置
run_path = sys.path.append(path)
# 执行spider命令进入调试的断点
execute(['scrapy', 'crawl', 'top250movie'])