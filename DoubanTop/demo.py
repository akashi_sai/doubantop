import requests

URL = 'https://movie.douban.com/top250'
HEADERS = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 ('
                  'KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36',
}


def test():
    # url = 'https://movie.douban.com/top250'
    resp = requests.get(url=URL, headers=HEADERS)
    print(resp.status_code)


if __name__ == '__main__':
    test()
