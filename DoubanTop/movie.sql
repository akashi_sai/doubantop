/*
 Navicat Premium Data Transfer

 Source Server         : host
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : localhost:3306
 Source Schema         : douban

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 25/09/2019 18:51:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for movie
-- ----------------------------
DROP TABLE IF EXISTS `movie`;
CREATE TABLE `movie` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '电影名称',
  `year` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '播放年份',
  `poster` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '海报地址',
  `context` varchar(1024) COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容',
  `score` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '评分',
  `intro` varchar(1024) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '简介',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

SET FOREIGN_KEY_CHECKS = 1;
