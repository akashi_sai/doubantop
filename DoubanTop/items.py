# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst


class DoubantopItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    table = 'movies'
    collection = 'movies'

    title = scrapy.Field()
    year = scrapy.Field()
    poster = scrapy.Field()
    # director = scrapy.Field()
    # script = scrapy.Field()
    # actor = scrapy.Field()
    # genre = scrapy.Field()
    # location = scrapy.Field()
    # language = scrapy.Field()
    # date = scrapy.Field()
    # timer = scrapy.Field()

    context = scrapy.Field()
    score = scrapy.Field()
    # intro = scrapy.Field()


class NewLoader(ItemLoader):
    default_output_processor = TakeFirst()

