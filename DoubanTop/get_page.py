import requests

url = 'https://movie.douban.com/subject/1292052/'
headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit'
                  '/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36'
}


def get_page():
    reponse = requests.get(url=url, headers=headers)
    with open('douban.html', 'w') as file:
        file.write(reponse.text)


if __name__ == '__main__':
    get_page()
