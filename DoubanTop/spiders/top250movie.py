# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.loader import ItemLoader
from scrapy.spiders import CrawlSpider, Rule

from DoubanTop.items import DoubantopItem, NewLoader


class Top250movieSpider(CrawlSpider):
    name = 'top250movie'
    allowed_domains = ['movie.douban.com']
    start_urls = ['https://movie.douban.com/top250']

    rules = (
        # Rule(LinkExtractor(allow=r'Items/'), callback='parse_item', follow=True),
        Rule(LinkExtractor(restrict_css='.grid_view .item .info .hd a'), callback='parse_item'),
        Rule(LinkExtractor(restrict_css='.paginator .next a'))
    )

    def parse_item(self, response):
        # item = {}
        #item['domain_id'] = response.xpath('//input[@id="sid"]/@value').get()
        #item['name'] = response.xpath('//div[@id="name"]').get()
        #item['description'] = response.xpath('//div[@id="description"]').get()
        loader = NewLoader(item=DoubantopItem(), response=response)
        loader.add_css('title', '#content h1 span::text')
        loader.add_css('year', '#content h1 .year::text')
        loader.add_css('poster', '#content #mainpic a img::attr("src")')

        # loader.add_css('director', 'script[type="application/ld+json"]')
        # loader.add_css('script', '#content #info span span.attrs a')
        #
        # loader.add_css('actor', '#content #info span.actor .attrs a')
        #
        # loader.add_css('genre', '#content ')
        # loader.add_css('location', '#content')
        # loader.add_css('language', '#content')
        # loader.add_css('date', '#content')
        # loader.add_css('timer', '#content')

        loader.add_css('context', 'script[type="application/ld+json"]::text')

        loader.add_css('score', '#interest_sectl .ll.rating_num::text')
        # loader.add_css('intro', '.indent#link-report .all.hidden::text')
        yield loader.load_item()
